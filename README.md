# DbD_Pingz
Tool to get your ping in DbD:

To get console output start DbDPingz with argument "-console".

Used libraries, services and content:
PcapDotNet, Newtonsoft JSON, Go Squared Flags, http://ipinfo.io API

## How to use?
Download this repository and build the solution either with Visual Studio or MSBUILD.exe. If you don't know how to do this just ask me for a build on Twitter: http://twitter.besentv.xyz ;)

 ### FAQ:
 * Why does Pingz not support IpV6? - Steam and DbD do not even work with pure IpV6...
